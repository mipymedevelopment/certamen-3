import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VistaCardComponent } from './pages/vista-card/vista-card.component';
import { ListaComponent } from './pages/lista/lista.component';
import { AdicionarComponent } from './pages/adicionar/adicionar.component';

const routes: Routes = [
  {path: '', redirectTo:'vistacard',pathMatch:'full'},
  {path:'vistacard', component:VistaCardComponent},
  {path:'lista', component:ListaComponent},
  {path:'adicionar',component:AdicionarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
