export interface Cliente {
    nombres:string,
    apellidos:string,
    genero:'hombre'|'mujer'|'otro',
    pais:string,
    ciudad:string
}
