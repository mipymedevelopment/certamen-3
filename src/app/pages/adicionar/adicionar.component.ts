import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ClientesService } from 'src/app/services/clientes.service';
import { Cliente } from 'src/app/interfaces/cliente';

@Component({
  selector: 'app-adicionar',
  templateUrl: './adicionar.component.html',
  styleUrls: ['./adicionar.component.scss']
})
export class AdicionarComponent implements OnInit {

  constructor(private CliServ: ClientesService) { }

  cliente:FormGroup = new FormGroup({
    name: new FormControl(''),
    lastName: new FormControl(''),
    genero: new FormControl('hombre'),
    pais: new FormControl('Chile'),
    ciudad: new FormControl('Valparaiso')
  })

  paises:Array<String> = ['Chile', 'Argentina', 'Colombia']
  ciudades:any = {Chile:['Valparaiso', 'RM'], Argentina:['Buenos Aires', 'Mendoza'],Colombia:['Bogota']} 

  showMessage:boolean = false

  ngOnInit(): void {

  }

  handleCreate(){
    let clientes:Array<Cliente> = []
    this.CliServ.clients.subscribe( cli => clientes=cli)   
    clientes.push({
      nombres:this.cliente.get('name')?.value,
      apellidos:this.cliente.get('lastName')?.value,
      genero:this.cliente.get('genero')?.value,
      pais:this.cliente.get('pais')?.value,
      ciudad:this.cliente.get('ciudad')?.value})
    
    this.CliServ.updateClients(clientes)
    this.resetForm()

    this.showMessage = true
    setTimeout(()=>{
      this.showMessage = false
    },1000)
  }

  resetForm(){
    this.cliente.patchValue({
      name:'',
      lastName:'',
      genero:'hombre',
      pais:'Chile',
      ciudad:'Valparaiso'
    })
  }
}
