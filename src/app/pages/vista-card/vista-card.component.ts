import { Component, OnInit } from '@angular/core';
import { ClientesService } from 'src/app/services/clientes.service';
import { Cliente } from 'src/app/interfaces/cliente';

@Component({
  selector: 'app-vista-card',
  templateUrl: './vista-card.component.html',
  styleUrls: ['./vista-card.component.scss']
})
export class VistaCardComponent implements OnInit {

  constructor(private CliServ: ClientesService) { }

  clientes: Array<Cliente> = []
  imgsrc: any = {hombre: "../../assets/profile-male.png",
                 mujer:"../../assets/profile-female.png",
                 otro:"../../assets/exis.jpg"}

  ngOnInit(): void {
    this.CliServ.clients.subscribe( cli => this.clientes=cli )
  }

}
