import { Component, OnInit } from '@angular/core';
import { ClientesService } from 'src/app/services/clientes.service';
import { Cliente } from 'src/app/interfaces/cliente';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  constructor(private CliServ: ClientesService) { }

  clientes: Array<Cliente> = []

  ngOnInit(): void {
    this.CliServ.clients.subscribe(cli => this.clientes=cli)
  }

}
