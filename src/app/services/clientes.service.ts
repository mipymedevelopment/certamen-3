import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Cliente } from '../interfaces/cliente';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  public clients = new BehaviorSubject<Array<Cliente>>([
    {nombres:'Friedrich August',apellidos:'von Hayek',genero:'hombre',pais:'chile',ciudad:'Valparaiso'},
    {nombres:'Aynd',apellidos:'Rand',genero:'mujer',pais:'chile',ciudad:'RM'}
  ]);

  constructor() { }

  updateClients(new_clients:Array<Cliente>){
    this.clients.next(new_clients)
  }

}
